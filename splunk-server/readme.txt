docker pull splunk/splunk

docker run -d -e "SPLUNK_START_ARGS=--accept-license" -e "SPLUNK_USER=root" -p "8000:8000" -p "15000:15000" -p "8088:8088" -p "8089:8089" splunk/splunk

go to localhost:8000
user: admin
pw: changeme

Useful guide
http://dev.splunk.com/view/splunk-logging-java/SP-CAAAE2K
http://dev.splunk.com/view/event-collector/SP-CAAAE7F
http://docs.splunk.com/Documentation/Splunk/latest/Data/UsetheHTTPEventCollector
http://dev.splunk.com/view/event-collector/SP-CAAAE7G

Follow instructions to set up Splunk for HTTP Event Collector
http://docs.splunk.com/Documentation/Splunk/latest/Data/UsetheHTTPEventCollector

Follow instructions to set up Splunk for TCP inputs
http://docs.splunk.com/Documentation/Splunk/latest/Data/Monitornetworkports

