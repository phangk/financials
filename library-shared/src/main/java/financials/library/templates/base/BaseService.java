package financials.library.templates.base;

import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Kevin Phang
 */
public abstract class BaseService {

//    protected String pathVariable;
    protected String requestBody;
    protected MultiValueMap<String, String> requestParam;
    protected MultiValueMap<String, String> requestHeader;
    protected Map<String, Object> dataStore;

    public BaseService() {
        this.dataStore = new HashMap<>();
    }

    protected abstract void circuitBreaker();

    protected abstract void validation();

    protected abstract void authorisationSecurityCheck();

    protected abstract void functionalSecurityCheck();

    protected abstract void generateResults();

    protected abstract void filterResults();

    public final Object execute(
            String requestBody,
            MultiValueMap<String, String> requestParam,
            MultiValueMap<String, String> requestHeader){
//            String pathVariable) {

//        this.pathVariable = pathVariable;
        this.requestBody = requestBody;
        this.requestParam = requestParam;
        this.requestHeader = requestHeader;
        this.circuitBreaker();
        this.validation();
        this.authorisationSecurityCheck();
        this.functionalSecurityCheck();
        this.generateResults();
        this.filterResults();

        if (this.dataStore.containsKey("returnedObject")) {
            return this.dataStore.get("returnedObject");

        } else {
            return null;

        }
    }
}
