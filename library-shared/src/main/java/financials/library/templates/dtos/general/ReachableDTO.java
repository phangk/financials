package financials.library.templates.dtos.general;


import financials.library.templates.dtos.reachable.EmailDTO;
import financials.library.templates.dtos.reachable.LocationDTO;
import financials.library.templates.dtos.reachable.PhoneDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Phang
 */
public class ReachableDTO {

    private List<EmailDTO> emailList;

    private List<LocationDTO> locationList;

    private List<PhoneDTO> phoneList;

    public List<EmailDTO> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<EmailDTO> emailList) {
        this.emailList = emailList;
    }

    public List<LocationDTO> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<LocationDTO> locationList) {
        this.locationList = locationList;
    }

    public List<PhoneDTO> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<PhoneDTO> phoneList) {
        this.phoneList = phoneList;
    }

    public void addEmail(EmailDTO email) {
        if (emailList == null) {
            emailList = new ArrayList<>();
        }

        emailList.add(email);
    }

    public void removeEmail(EmailDTO email) {
        if (emailList != null) {
            if (emailList.contains(email)) {
                emailList.remove(email);
            }
        }
    }

    public void addPhone(PhoneDTO phone) {
        if (phoneList == null) {
            phoneList = new ArrayList<>();
        }

        phoneList.add(phone);
    }

    public void removePhone(PhoneDTO phone) {
        if (phoneList != null) {
            if (phoneList.contains(phone)) {
                phoneList.remove(phone);
            }
        }
    }

    public void addLocation(LocationDTO location) {
        if (locationList == null) {
            locationList = new ArrayList<>();
        }

        locationList.add(location);
    }

    public void removeLocation(LocationDTO location) {
        if (locationList != null) {
            if (locationList.contains(location)) {
                locationList.remove(location);
            }
        }
    }

}
