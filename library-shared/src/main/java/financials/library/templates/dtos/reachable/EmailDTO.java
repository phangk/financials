package financials.library.templates.dtos.reachable;


import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Kevin Phang
 */
public class EmailDTO {

    @JsonIgnore
    private String primaryKey;
    private String email;
    private String type;

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
