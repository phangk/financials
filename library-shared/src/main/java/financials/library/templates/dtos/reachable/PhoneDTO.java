package financials.library.templates.dtos.reachable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Kevin Phang
 */

public class PhoneDTO {

    @JsonIgnore
    private String primaryKey;
    private String number;
    private String areaCode;
    private String type;

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
