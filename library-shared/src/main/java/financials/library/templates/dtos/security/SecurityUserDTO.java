//package financials.library.templates.dtos.security;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author Kevin Phang
// */
//public class SecurityUserDTO {
//
//    private String primaryKey;
//    private String username;
//    private String password;
//    private boolean enabled;
//    private boolean credentialsNonExpired;
//    private boolean accountNonLocked;
//    private boolean accountNonExpired;
//
//    private List<SecurityAuthorityDTO> securityAuthorities;
//
//    public String getPrimaryKey() {
//        return primaryKey;
//    }
//
//    public void setPrimaryKey(String primaryKey) {
//        this.primaryKey = primaryKey;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public boolean isEnabled() {
//        return enabled;
//    }
//
//    public void setEnabled(boolean enabled) {
//        this.enabled = enabled;
//    }
//
//    public List<SecurityAuthorityDTO> getSecurityAuthorities() {
//        return securityAuthorities;
//    }
//
//    public void setSecurityAuthorities(List<SecurityAuthorityDTO> securityAuthorities) {
//        this.securityAuthorities = securityAuthorities;
//    }
//
//    public void addSecurityAuthority(SecurityAuthorityDTO securityAuthorityDTO) {
//        securityAuthorityDTO.setSecurityUserDTO(this);
//        if (securityAuthorities == null) {
//            securityAuthorities = new ArrayList<>();
//        }
//
//        securityAuthorities.add(securityAuthorityDTO);
//    }
//
//    public void removeSecurityAuthority(SecurityAuthorityDTO securityAuthorityDTO) {
//        if (securityAuthorities != null) {
//            if (securityAuthorities.contains(securityAuthorityDTO)) {
//                securityAuthorities.remove(securityAuthorityDTO);
//            }
//        }
//    }
//
//    public boolean isCredentialsNonExpired() {
//        return credentialsNonExpired;
//    }
//
//    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
//        this.credentialsNonExpired = credentialsNonExpired;
//    }
//
//    public boolean isAccountNonLocked() {
//        return accountNonLocked;
//    }
//
//    public void setAccountNonLocked(boolean accountNonLocked) {
//        this.accountNonLocked = accountNonLocked;
//    }
//
//    public boolean isAccountNonExpired() {
//        return accountNonExpired;
//    }
//
//    public void setAccountNonExpired(boolean accountNonExpired) {
//        this.accountNonExpired = accountNonExpired;
//    }
//}
//
