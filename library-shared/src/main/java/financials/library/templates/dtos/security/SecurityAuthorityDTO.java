//package financials.library.templates.dtos.security;
//
//import org.springframework.security.core.GrantedAuthority;
//
///**
// * @author Kevin Phang
// */
//public class SecurityAuthorityDTO implements GrantedAuthority {
//
//    private String primaryKey;
//    private String authority;
//    private SecurityUserDTO securityUserDTO;
//
//    public String getPrimaryKey() {
//        return primaryKey;
//    }
//
//    public void setPrimaryKey(String primaryKey) {
//        this.primaryKey = primaryKey;
//    }
//
//    public String getAuthority() {
//        return authority;
//    }
//
//    public void setAuthority(String authority) {
//        this.authority = authority;
//    }
//
//    public SecurityUserDTO getSecurityUserDTO() {
//        return securityUserDTO;
//    }
//
//    public void setSecurityUserDTO(SecurityUserDTO securityUserDTO) {
//        this.securityUserDTO = securityUserDTO;
//    }
//}
