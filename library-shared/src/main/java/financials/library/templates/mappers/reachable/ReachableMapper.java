package financials.library.templates.mappers.reachable;

import financials.library.entities.reachable.EmailConnectorDTO;
import financials.library.entities.reachable.LocationConnectorDTO;
import financials.library.entities.reachable.PhoneConnectorDTO;
import financials.library.templates.dtos.reachable.EmailDTO;
import financials.library.templates.dtos.reachable.LocationDTO;
import financials.library.templates.dtos.reachable.PhoneDTO;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author Kevin Phang
 */
@Component
public class ReachableMapper extends DozerBeanMapper {

    public static BeanMappingBuilder reachableMappingBuilder = new BeanMappingBuilder() {

        @Override
        protected void configure() {

            // IMPORTANT NOTE: Excludes are to avoid circular references

            mapping(EmailConnectorDTO.class, EmailDTO.class).fields("entityId", "primaryKey");

            mapping(LocationConnectorDTO.class, LocationDTO.class).fields("entityId", "primaryKey");

            mapping(PhoneConnectorDTO.class, PhoneDTO.class).fields("entityId", "primaryKey");

        }
    };

    public ReachableMapper() {
        addMapping(reachableMappingBuilder);
    }

    @Bean
    public ReachableMapper getApplicationMapper() {
        return new ReachableMapper();
    }
}

