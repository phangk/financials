//package financials.library.templates.mappers.security;
//
//import financials.library.entities.security.SecurityAuthorityConnectorDTO;
//import financials.library.templates.dtos.security.SecurityAuthorityDTO;
//import org.dozer.DozerBeanMapper;
//import org.dozer.loader.api.BeanMappingBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//
//import static financials.library.templates.mappers.security.SecurityUserMapper.securityUserMappingBuilder;
//
///**
// * @author Kevin Phang
// */
//@Component
//public class SecurityAuthorityMapper extends DozerBeanMapper {
//
//    public static BeanMappingBuilder securityAuthorityMappingBuilder = new BeanMappingBuilder() {
//
//        @Override
//        protected void configure() {
//
//            mapping(SecurityAuthorityConnectorDTO.class, SecurityAuthorityDTO.class).fields("entityId", "primaryKey");
//
//        }
//    };
//
//    public SecurityAuthorityMapper() {
//        addMapping(securityAuthorityMappingBuilder);
//        addMapping(securityUserMappingBuilder);
//    }
//
//    @Bean
//    public SecurityAuthorityMapper getApplicationMapper() {
//        return new SecurityAuthorityMapper();
//    }
//}
