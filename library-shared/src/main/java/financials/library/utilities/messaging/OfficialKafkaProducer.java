package financials.library.utilities.messaging;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.Properties;

/**
 * @author Kevin Phang
 */
@Configuration
public class OfficialKafkaProducer {

    @Value("${infra.analytics.engine.host}")
    private String infraAnalyticsEngineHost;

    @Value("${apache.kafka.broker-url}")
    private String kafkaBrokerUrl;

    public void send(String topic, String value) {
        Properties properties = new Properties();
//        properties.put("bootstrap.servers", infraAnalyticsEngineHost + ":9092");
        properties.put("bootstrap.servers", kafkaBrokerUrl);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

//        properties.put("acks", "all");
//        properties.put("retries", 0);
//        properties.put("batch.size", 16384);
//        properties.put("linger.ms", 1);
//        properties.put("buffer.memory", 33554432);

        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<String, String>(properties);
        try {
            kafkaProducer.send(new ProducerRecord<String, String>(topic, value));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            kafkaProducer.close();
        }

    }


}
