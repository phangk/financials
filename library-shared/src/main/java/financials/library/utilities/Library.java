package financials.library.utilities;

import financials.library.utilities.logger.FluentService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author Kevin Phang
 */
@EnableAsync
@Configuration
public class Library {

    public static final String DOMAIN_COMPANY = "/eydemo";
    public static final String DOMAIN_SUFFIX_FINANCIALS = DOMAIN_COMPANY + "/financials";
    public static final String DOMAIN_SUFFIX_BANKING = DOMAIN_COMPANY + "/banking";
    public static final String DOMAIN_SUFFIX_INSURANCE = DOMAIN_COMPANY + "/insurance";

    public static final DateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    public static final DateFormat formatDateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Bean
    public FluentService fluentService() {
        return new FluentService();
    }

}
