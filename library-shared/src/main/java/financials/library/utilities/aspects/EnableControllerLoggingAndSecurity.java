package financials.library.utilities.aspects;

/**
 * @author Kevin Phang
 */
public @interface EnableControllerLoggingAndSecurity {
}
