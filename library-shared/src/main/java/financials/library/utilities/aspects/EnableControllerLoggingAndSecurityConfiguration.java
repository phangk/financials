package financials.library.utilities.aspects;

import com.fasterxml.jackson.databind.ObjectMapper;
import financials.library.exception.BaseExceptionHelper;
import financials.library.utilities.logger.FluentService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author Kevin Phang
 */
@Aspect
@Component
public class EnableControllerLoggingAndSecurityConfiguration {

    @Autowired
    private FluentService fluentService;

    //    @Async
//    public Future<Object> aroundMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    @Around("execution(@financials.library.utilities.aspects.EnableControllerLoggingAndSecurity * *(..))")
    public Object aroundMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

//        try {

            long startTime = System.currentTimeMillis();

//            String pathVariable = null;
            String requestBody = null;
            MultiValueMap<String, String> requestParam = null;
            MultiValueMap<String, String> requestHeader = null;

            Map<String, Object> logData = new HashMap<>();
            ObjectMapper objectMapper = new ObjectMapper();

            // Get necessary information
            Signature signature = proceedingJoinPoint.getSignature();
            Object[] requestData = proceedingJoinPoint.getArgs();

            String methodLocation = String.format("%s.%s", signature.getDeclaringTypeName(), signature.getName());

            MethodSignature methodSignature = (MethodSignature) signature;
            Method method = methodSignature.getMethod();

            Annotation[][] parameterAnnotations = method.getParameterAnnotations();

            for (int argIndex = 0; argIndex < parameterAnnotations.length; argIndex++) {
                for (Annotation annotation : parameterAnnotations[argIndex]) {
                    if (annotation instanceof RequestBody) {
                        requestBody = (String) requestData[argIndex];

                    } else if (annotation instanceof RequestParam) {
                        requestParam = (MultiValueMap<String, String>) requestData[argIndex];

                    } else if (annotation instanceof RequestHeader) {
                        requestHeader = (MultiValueMap<String, String>) requestData[argIndex];

                    }

//                    else if (annotation instanceof PathVariable) {
//                        pathVariable = (String) requestData[argIndex];
//
//                    }

                    else {
                        continue;
                    }
                }
            }

            String apiBase = null;
            String apiSuffix = null;
            String requestMethod = null;

            Annotation[] classAnnotations = method.getDeclaringClass().getAnnotations();

            for (int argIndex = 0; argIndex < classAnnotations.length; argIndex++) {
                Annotation annotation = classAnnotations[argIndex];
                if (annotation instanceof RequestMapping) {
                    try {
                        RequestMapping requestMapping = (RequestMapping) annotation;
                        apiBase = requestMapping.value()[0];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        continue;
                    }
                }
            }

            Annotation[] methodAnnotations = method.getAnnotations();

            for (int argIndex = 0; argIndex < methodAnnotations.length; argIndex++) {
                Annotation annotation = methodAnnotations[argIndex];
                if (annotation instanceof RequestMapping) {
                    try {
                        RequestMapping requestMapping = (RequestMapping) annotation;
                        apiSuffix = requestMapping.value()[0];
                        requestMethod = String.valueOf(requestMapping.method()[0]);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        continue;
                    }
                }
            }

            Object responseObject = proceedingJoinPoint.proceed(requestData);

            // Generate and send fluent logs
            long endTime = System.currentTimeMillis();

            logData.put(FluentService.LOG_LEVEL, String.valueOf(Level.INFO));
            logData.put(FluentService.LOG_LOCATION, methodLocation);
            logData.put(FluentService.LOG_REQUEST_MAPPING, String.format("%s%s", apiBase, apiSuffix));
            logData.put(FluentService.LOG_REQUEST_METHOD, requestMethod);
            logData.put(FluentService.LOG_EXECUTION_TIME_MS, endTime - startTime);
            logData.put(FluentService.LOG_REQUEST_BODY, String.valueOf(requestBody));
            logData.put(FluentService.LOG_REQUEST_PARAM, String.valueOf(requestParam));
            logData.put(FluentService.LOG_REQUEST_HEADER, String.valueOf(requestHeader));
            logData.put(FluentService.LOG_RESPONSE_STATUS, HttpStatus.OK);
            logData.put(FluentService.LOG_RESPONSE_OBJECT, objectMapper.writeValueAsString(responseObject));

            fluentService.log(logData);

            return method.getReturnType().cast(responseObject);
//        return new AsyncResult<>(method.getReturnType().cast(responseObject));
//        } catch (Exception e) {
//            // TODO: Re-enable this during production
//            BaseExceptionHelper.throwCleanedException(e);
//            return new Object();
//        }
    }
}
