package financials.library.utilities.logger;

import org.fluentd.logger.FluentLogger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Kevin Phang
 */
@Service
public class FluentService {

    // Required
    public final static String LOG_LEVEL = "logging_level";
    public final static String LOG_LOCATION = "logging_method_location";

    // Controller specific
    public final static String LOG_REQUEST_METHOD = "request_method";
    public final static String LOG_REQUEST_MAPPING = "request_mapping";
    public final static String LOG_REQUEST_BODY = "request_body";
    public final static String LOG_REQUEST_PARAM = "request_param";
    public final static String LOG_REQUEST_HEADER = "request_header";
    public final static String LOG_RESPONSE_STATUS = "response_status";
    public final static String LOG_RESPONSE_OBJECT = "response_object";
    public final static String LOG_EXECUTION_TIME_MS = "execution_time_ms";

    // Others
    public final static String LOG_METHOD_PARAMS = "logging_method_args";

    // Exceptions
    public final static String LOG_EXCEPTION_MESSAGE = "exception_message";
    public final static String LOG_EXCEPTION_TYPE = "exception_type";
    public final static String LOG_EXCEPTION_TRACE = "exception_trace";

    @Value("${spring.application.name}")
    public String springApplicationName;

    @Value("${fluent.logger.name}")
    private String fluentLoggerName;

    @Value("${fluent.logger.host}")
    private String fluentLoggerHost;

    @Value("${fluent.logger.port}")
    private int fluentLoggerPort;

    public void log(Map<String, Object> hashMap) {
        FluentLogger.getLogger(fluentLoggerName, fluentLoggerHost, fluentLoggerPort).log(springApplicationName, hashMap);
    }


}
