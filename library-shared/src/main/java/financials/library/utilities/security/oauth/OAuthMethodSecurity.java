//package financials.library.utilities.security.oauth;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
//import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;
//import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
//
///**
// * @author Kevin Phang
// */
//@Configuration
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class OAuthMethodSecurity extends GlobalMethodSecurityConfiguration {
//
//    @Autowired
//    private ResourceServerProperties sso;
//
//    @Override
//    protected MethodSecurityExpressionHandler createExpressionHandler() {
//        return new OAuth2MethodSecurityExpressionHandler();
//    }
//
//    @Bean
//    public ResourceServerTokenServices oAuthTokenService() {
//        return new OAuthTokenService(sso.getUserInfoUri(), sso.getClientId());
//    }
//
//}
