package financials.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Not authorized")
public class SecurityException extends RuntimeException {

    public SecurityException() {
        super();
    }

    public SecurityException(String msg) {
        super(msg);
    }

    public SecurityException(Exception e) {
        super(e);
    }
}