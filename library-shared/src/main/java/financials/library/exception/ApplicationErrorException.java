package financials.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Application error")
public class ApplicationErrorException extends RuntimeException {

    public ApplicationErrorException() {
        super();
    }

    public ApplicationErrorException(String msg) {
        super(msg);
    }

    public ApplicationErrorException(Exception e) {
        super(e);
    }

}