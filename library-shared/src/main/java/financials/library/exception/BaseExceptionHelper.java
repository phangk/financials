package financials.library.exception;

public class BaseExceptionHelper {

    // Method to remove the exception message before sending to API Gateway
    // To ensure API doesn't send that message to the browser potentially giving important security information to the end client
    public static void throwCleanedException(Exception e) {
        if (e instanceof ResourceNotFoundException) {
            throw new ResourceNotFoundException();
        } else if (e instanceof SecurityException) {
            throw new SecurityException();
        } else if (e instanceof ValidationException) {
            throw new ValidationException();
        } else {
            throw new ApplicationErrorException();
        }
    }
}
