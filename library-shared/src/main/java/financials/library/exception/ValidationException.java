package financials.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Unapproved object")
public class ValidationException extends RuntimeException {

    public ValidationException() {
        super();
    }

    public ValidationException(String msg) {
        super(msg);
    }

}
