package com.ey.alipay.poc.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ey.alipay.poc.order.service.GetService;
import financials.library.utilities.aspects.EnableControllerLoggingAndSecurity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import com.ey.alipay.poc.order.model.Order;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController()
@RequestMapping(value="/order")
public class OrderController {

	@Autowired
	GetService getService;
	
	@RequestMapping(value = "/available")
	public String available() {
		return "Available is called.";
	 }
	
//	@HystrixCommand(fallbackMethod = "fallback", groupKey = "Order",
//			commandKey = "order", threadPoolKey = "orderThread")

	@EnableControllerLoggingAndSecurity
	@RequestMapping(method = RequestMethod.GET)
	public HttpEntity<Order> getOrder(@RequestBody(required = false) String requestBody,
									  @RequestParam(required = false) MultiValueMap<String, String> requestParam,
									  @RequestHeader(required = false) MultiValueMap<String, String> requestHeader) {

		return new ResponseEntity<>((Order) getService.execute(requestBody, requestParam, requestHeader), HttpStatus.OK);
	}
	
//	public Order fallback(Long id) {
//		System.out.println("Fall back from order. Id:"+id);
//		return new Order();
//    }
	
}
