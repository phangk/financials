package com.ey.alipay.poc.order.model;

public class Order {
	private Long id;
	private String customerName;
	
	public Order(Long id, String customerName) {
		super();
		this.id = id;
		this.customerName = customerName;
	}
	public Order() {
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	

}