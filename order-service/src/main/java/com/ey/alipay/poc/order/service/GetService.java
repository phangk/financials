package com.ey.alipay.poc.order.service;

import com.ey.alipay.poc.order.model.Order;
import financials.library.templates.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Kevin Phang
 */
@Service
public class GetService extends BaseService {

    private Map<Long, String> map;

    public GetService() {
        map = new HashMap<>();
        map.put(1L, "A-01");
        map.put(2L, "A-02");
    }

    @Override
    protected void circuitBreaker() {

    }

    @Override
    protected void validation() {

    }

    @Override
    protected void authorisationSecurityCheck() {

    }

    @Override
    protected void functionalSecurityCheck() {

    }

    @Override
    protected void generateResults() {

        Long id = Long.valueOf(requestParam.get("id").get(0));
//        Long id = Long.valueOf(pathVariable);

        String url = "http://poc-customer-dev.us-east-1.elasticbeanstalk.com/customer/name/" + map.get(id);
        System.out.println("url:"+url);

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> customerServiceResponse = restTemplate.exchange(url,
                HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
                });
        final String customerName = customerServiceResponse.getBody();

        dataStore.put("returnedObject", new Order(id, customerName));

    }

    @Override
    protected void filterResults() {

    }
}
