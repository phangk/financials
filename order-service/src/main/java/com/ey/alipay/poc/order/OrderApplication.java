package com.ey.alipay.poc.order;

import financials.library.utilities.Library;
import financials.library.utilities.database.MultipleDatabase;
import financials.library.utilities.database.SimpleDatabase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;

@EnableCircuitBreaker
//@EnableDiscoveryClient
//@EnableEurekaClient
@SpringBootApplication
@EnableAutoConfiguration(exclude = {
        SecurityAutoConfiguration.class,
        ManagementWebSecurityAutoConfiguration.class,
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class})
@Import({Library.class})
@ComponentScan(value={"financials", "com.ey"}, excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value=MultipleDatabase.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value=SimpleDatabase.class)})
public class OrderApplication {

	public static void main(String[] args) {
    SpringApplication.run(OrderApplication.class, args);
  }
}
