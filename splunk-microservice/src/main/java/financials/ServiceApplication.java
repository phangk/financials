package financials;

import com.splunk.HttpService;
import com.splunk.SSLSecurityProtocol;
import com.splunk.Service;
import com.splunk.logging.SplunkCimLogEvent;
import com.splunk.logging.TcpAppender;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import financials.library.utilities.Library;
import financials.library.utilities.database.MultipleDatabase;
import financials.library.utilities.database.SimpleDatabase;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Kevin Phang
 */
//@EnableResourceServer
//@EnableGlobalMethodSecurity(prePostEnabled = true)
@SpringBootApplication
@ComponentScan(value="financials", excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value=MultipleDatabase.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value=SimpleDatabase.class)})
@Import({Library.class})
@EnableAutoConfiguration(exclude = {
        SecurityAutoConfiguration.class,
        ManagementWebSecurityAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class
        })
public class ServiceApplication implements CommandLineRunner {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ServiceApplication.class, args);

        LoggerContext loggerContext = new LoggerContext("splunkLogger");
        loggerContext.reconfigure();
        loggerContext.updateLoggers();

        Logger logger = loggerContext.getLogger("splunkLogger");
        logger.info("This is a test event for Logback test");

    }

    @Override
    public void run(String... strings) throws Exception {



    }

}
