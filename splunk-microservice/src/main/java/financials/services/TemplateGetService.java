package financials.services;

import financials.library.templates.base.BaseService;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

/**
 * @author Kevin Phang
 */
@Service
public class TemplateGetService extends BaseService {

    @Override
    protected void circuitBreaker() {

    }

    @Override
    protected void validation() {

    }

    @Override
    protected void authorisationSecurityCheck() {

    }

    @Override
    protected void functionalSecurityCheck() {

    }

    @Override
    protected void generateResults() {

        String receivedRequestParam = requestParam.get("").get(0);
        String receivedRequestBody = new JSONObject(requestBody).toString();
        String receivedRequestHeader = String.valueOf(requestHeader.get(""));

        dataStore.put("returnedObject", "");

    }

    @Override
    protected void filterResults() {

    }
}
