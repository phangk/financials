package financials.controllers;

import com.splunk.Service;
import financials.library.utilities.aspects.EnableControllerLoggingAndSecurity;
import financials.services.TemplateGetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

import static financials.controllers.TemplateController.API_BASE;
import static financials.library.utilities.Library.DOMAIN_SUFFIX_FINANCIALS;


/**
 * @author Kevin Phang
 */
// TODO: Remove @CrossOrigin during deployment
@RefreshScope
@RestController()
@RequestMapping(value = DOMAIN_SUFFIX_FINANCIALS + API_BASE)
@CrossOrigin()
public class TemplateController {

//    @Autowired
//    private Service splunkService;

    // Change import statement when replicating
    static final String API_BASE = "/apis/splunk";
    private static final String API_SPECIFIC = "/test";

    @Autowired
    TemplateGetService templateGetService;

    @EnableControllerLoggingAndSecurity
    @RequestMapping(value = API_SPECIFIC, method = RequestMethod.GET)
    public HttpEntity<String> getInfo(@RequestBody(required = false) String requestBody,
                                      @RequestParam(required = false) MultiValueMap<String, String> requestParam,
                                      @RequestHeader(required = false) MultiValueMap<String, String> requestHeader) throws ParseException {

        return new ResponseEntity<>((String) templateGetService.execute(requestBody, requestParam, requestHeader), HttpStatus.OK);
    }

}
